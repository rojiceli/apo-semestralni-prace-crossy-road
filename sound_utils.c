#include "sound_utils.h"

uint32_t get_period_oct3(char note);
uint32_t get_period_oct4(char note);
uint32_t get_period_oct5(char note);
uint32_t get_period_oct6(char note);
void *play_sound_effect_train();
void *play_sound_effect_gameover();
void *play_song_epicsax(void *vargp);
void *play_song_harrypotter(void *vargp);
void play_note_song(char note, int octave, int millis);
void delay_millis(int millis);

unsigned char *mem_base_sound;
bool playing = false;
pthread_t thread_song;
pthread_t thread_effect;

void init_sound(){
    mem_base_sound = map_phys_address(AUDIOPWM_REG_BASE_PHYS, AUDIOPWM_REG_SIZE, 0);
    *(volatile uint32_t*)(mem_base_sound + AUDIOPWM_REG_CR_o) = 1;
    *(volatile uint32_t*)(mem_base_sound + AUDIOPWM_REG_PWM_o) = 0;
}

void stop_sound(){
    playing = false;
    pthread_join(thread_song, NULL);
}

void play_song(int song_id) {
    if(playing) return;
    switch (song_id) {
        case 0: pthread_create(&thread_song, NULL, play_song_epicsax, NULL); break;
        case 1: pthread_create(&thread_song, NULL, play_song_harrypotter, NULL); break;
        default: return;
    }
    playing = true;
}

void play_note_song(char note, int octave, int millis){
    if (!playing) return;
    play_note(note, octave, millis);
}

void play_note(char note, int octave, int millis){
    *(volatile uint32_t*)(mem_base_sound + AUDIOPWM_REG_PWM_o) = VOLUME;
    if(note != ' '){
        uint32_t period = 0;
        switch (octave) {
            case 3: period = get_period_oct3(note); break;
            case 4: period = get_period_oct4(note); break;
            case 5: period = get_period_oct5(note); break;
            case 6: period = get_period_oct6(note); break;
            default: fprintf(stderr, "Octave unsupported. New octaves comming soon...\n"); break;
        }
        *(volatile uint32_t*)(mem_base_sound + AUDIOPWM_REG_PWMPER_o) = period;
    } else *(volatile uint32_t*)(mem_base_sound + AUDIOPWM_REG_PWM_o) = 0;

    delay_millis(millis);
    *(volatile uint32_t*)(mem_base_sound + AUDIOPWM_REG_PWM_o) = 0;
    delay_millis(10);
}

void delay_millis(int millis){
    struct timespec delay;
    delay.tv_sec = millis / 1000;
    delay.tv_nsec = (millis % 1000) * 1000000;
    nanosleep(&delay, NULL);
}

void sound_effect_train(){
    pthread_create(&thread_effect, NULL, play_sound_effect_train, NULL);
}

void sound_effect_gameover(){
    pthread_create(&thread_effect, NULL, play_sound_effect_gameover, NULL);
}

void *play_sound_effect_train(){
    for (size_t i = 0; i < 3; i++) {
        play_note('E', 5, 200);
        play_note('C', 5, 200);
        play_note(' ', 5, 100);
    }
    return NULL;
}

void *play_sound_effect_gameover(){
    play_note('D', 4, 600);
    play_note('c', 4, 600);
    play_note('C', 4, 600);
    play_note('B', 3, 2000);
    return NULL;
}

void *play_song_epicsax(void *vargp) {
    while (playing) {
        play_note_song('A', 4, 500);
        play_note_song(' ', 4, 500);
        play_note_song('A', 4, 250);
        play_note_song('A', 4, 125);
        play_note_song('A', 4, 125);
        play_note_song('G', 4, 125);
        play_note_song('A', 4, 125);
        play_note_song(' ', 4, 250);
        play_note_song('A', 4, 500);
        play_note_song(' ', 4, 500);
        play_note_song('A', 4, 250);
        play_note_song('A', 4, 125);
        play_note_song('A', 4, 125);
        play_note_song('G', 4, 125);
        play_note_song('A', 4, 125);
        play_note_song(' ', 4, 250);
        play_note_song('A', 4, 500);
        play_note_song(' ', 4, 250);
        play_note_song('C', 4, 500);
        play_note_song('A', 4, 500);
        play_note_song('G', 4, 500);
        play_note_song('F', 4, 500);
        play_note_song('D', 4, 250);
        play_note_song('D', 4, 250);
        play_note_song('E', 4, 250);
        play_note_song('F', 4, 250);
        play_note_song('D', 4, 250);
    }
    return NULL;
}

void *play_song_harrypotter(void *vargp) {
    while (playing) {   
        play_note_song('B', 4, 500);
        play_note_song('E', 5, 750);
        play_note_song('G', 5, 250);
        play_note_song('f', 5, 500);
        play_note_song('E', 5, 1000);
        play_note_song('B', 5, 500);
        play_note_song('A', 5, 1500);
        play_note_song('f', 5, 1500);
        play_note_song('E', 5, 750);
        play_note_song('G', 5, 250);
        play_note_song('f', 5, 500);
        play_note_song('d', 5, 1000);
        play_note_song('F', 5, 500);
        play_note_song('C', 4, 2500);
        play_note_song('B', 4, 500);

        play_note_song('E', 5, 750);
        play_note_song('G', 5, 250);
        play_note_song('f', 5, 500);
        play_note_song('E', 5, 1000);
        play_note_song('B', 5, 500);
        play_note_song('D', 6, 1000);
        play_note_song('c', 6, 500);
        play_note_song('C', 6, 1000);
        play_note_song('g', 5, 500);
        play_note_song('C', 6, 750);
        play_note_song('B', 5, 250);
        play_note_song('a', 5, 500);
        play_note_song('a', 4, 1000);
        play_note_song('G', 5, 500);
        play_note_song('E', 5, 2500);
        play_note_song('G', 5, 500);
    }
    
    return NULL;
}

uint32_t get_period_oct3(char note){
    int period = 0;
    switch (note) {
        case 'C': period = 7644675; break;
        case 'c': period = 7215527; break;
        case 'D': period = 6810597; break;
        case 'd': period = 6428387; break;
        case 'E': period = 6067592; break;
        case 'F': period = 5727048; break;
        case 'f': period = 5405405; break;
        case 'G': period = 5102040; break;
        case 'g': period = 4815795; break;
        case 'A': period = 4545454; break;
        case 'a': period = 4290372; break;
        case 'B': period = 4049566; break;
        default:
            fprintf(stderr, "Invalid note!\n");
            break;
    }
    return period / 10;
}

uint32_t get_period_oct4(char note){
    int period = 0;
    switch (note) {
        case 'C': period = 3822192; break;
        case 'c': period = 3607764; break;
        case 'D': period = 3405299; break;
        case 'd': period = 3214091; break;
        case 'E': period = 3033704; break;
        case 'F': period = 2863442; break;
        case 'f': period = 2702776; break;
        case 'G': period = 2551020; break;
        case 'g': period = 2407898; break;
        case 'A': period = 2272727; break;
        case 'a': period = 2145186; break;
        case 'B': period = 2024783; break;
        default:
            fprintf(stderr, "Invalid note!\n");
            break;
    }
    return period / 10;
}

uint32_t get_period_oct5(char note){
    int period = 0;
    switch (note) {
        case 'C': period = 1911132; break;
        case 'c': period = 1803849; break;
        case 'D': period = 1702620; break;
        case 'd': period = 1607071; break;
        case 'E': period = 1516875; break;
        case 'F': period = 1431721; break;
        case 'f': period = 1351370; break;
        case 'G': period = 1275526; break;
        case 'g': period = 1203934; break;
        case 'A': period = 1136364; break;
        case 'a': period = 1072582; break;
        case 'B': period = 1012381; break;
        default:
            fprintf(stderr, "Invalid note!\n");
            break;
    }
    return period / 10;
}

uint32_t get_period_oct6(char note){
    int period = 0;
    switch (note) {
        case 'C': period = 955566; break;
        case 'c': period = 901932; break;
        case 'D': period = 851310; break;
        case 'd': period = 803529; break;
        case 'E': period = 758431; break;
        case 'F': period = 715865; break;
        case 'f': period = 675684; break;
        case 'G': period = 637763; break;
        case 'g': period = 601967; break;
        case 'A': period = 568181; break;
        case 'a': period = 536290; break;
        case 'B': period = 506193; break;
        default:
            fprintf(stderr, "Invalid note!\n");
            break;
    }
    return period / 10;
}


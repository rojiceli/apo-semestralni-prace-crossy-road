#ifndef GAME
#define GAME

#include "knobs_utils.h"
#include "light_utils.h"
#include "sound_utils.h"
#include "draw_utils.h"
#include "consts.h"
#include "other_windows.h"
#include <time.h>
#include <stdlib.h>

/** @brief First inicialize the game, then run the while cyclus. 
 * In each cycle update the position of chicken and obstacles (if needed) and redraw in on the screen.
 * When chicken collides with obstacle ends the game and show the game over window.
 * 
 */
void run();

#endif

# Crossy Road

Game Crossy Road for device mzAPO.

## How to run the game
Download all files from this git repository and upload them to your mzAPO device. Compile it and run with Makefile as "TARGET_IP=192.168.223.xxx make run" or modify the Makefile directly (line 31). After these steps, the main menu should appear on the screen of your mzAPO. 
 

 __How to play the game__

In the main menu, you can move around by turning the green knob, and by pressing it, you confirm the currently selected option. The game is controlled by turning the red and blue knobs (red for horizontal movement, blue for vertical movement). The goal of the game is to cross as many rows as possible with your player character, the chicken. However, beware of the dangers ahead, as you could get crushed by a train or a car or drown in the river unless you use a log to cross it.

## Project structure

The project is divided into several files, namely the main file (crossy_road.c) and several other supplementary libraries. 

__consts.h__

Before the more technical libraries, there is the consts header file where all the essential constants are declared. From those, these are the notable ones:
* `VOLUME` - change this for setting how loud the music and the sound effects should be
* `IMG_FOLDER` and `HIGH_SCORE_FILE` - please change the path to the one linked to your name. (substitute ".../houdefi1/..." by your username)

__game.c__

This library contains the game's logic functions. There is only one 'public' function, `void run()`, which is called when starting the game from the main menu. It initializes the game first and then runs the while cycle. In each cycle, it updates the position of the chicken and any obstacles if necessary. After that, it redraws the screen with the updated data. When the chicken collides with an obstacle, the game ends, and the game-over window is shown.
* `void update_obstacles()` - update layout 2D array of obstacles (move them in the right direction when they should be moved)
* `void move_background_layout()` - move the background behind the chicken. Generate a new row at the top and shift all rows one position down, discard the bottom row.
* `void move_obstacles_background()` - move obstacles same way as `move_background_layout()` move background.
* `void shift_obstacles_right(obstacle *new_obstacle, int row)` - move obstacles in the row from right to left.
* `void shift_obstacles_left(obstacle *new_obstacle, int row)` - move obstacles in a row from left to right.
* `bool check_collision()` - check if the chicken collides with an obstacle.

<br>

__draw_utils.c__

This library contains functions supporting drawing on the screen. Essential functions are `init_display()` for initialization and `update_screen()`, which redraws the display with the changes stored in the buffer. In addition, there are more functions for drawing on the screen. 


* `uint16_t convert_color(uint8_t red, uint8_t green, uint8_t blue)` - converts a color from classic 16bit rgb to rgb565
* `void draw_image(int x, int y, char *filename)` - draws an image with a given name into the buffer. The top left corner is at the given coordinates (use `draw_image_fliped(int x, int y, char *filename)` for drawing the image flipped vertically)
* `void draw_background_image()` - draws the background image into the buffer. Used mainly in menus
* `void fill_rectangle(double x, double y, double width, double height, uint16_t color)` - draws a filled rectangle into the buffer
* `void fill_triangle(int x, int y, int height, uint16_t color)` - draws a filled triangle ▶ into the buffer

* `void draw_string(double x, double y, char *str, Font *font, uint16_t color)` - draws a string into the buffer with set position, font_descriptor, size, and color
* `void draw_string_centered(double y, char *str, Font *font, uint16_t color)` - draws a string centered in the buffer with y position, font, and color
* `void draw_label(Label *label)` - draws a label into the buffer

* `void draw_game_tiles(square *layout[8][12])` - draws the background tiles of the game based on the actual layout of the background into the buffer 
* `void draw_obstacles(obstacle *obst_layout[8][12], square* sq_layout[8][12])` - draws the obstacles on the game background based on actual positions of obstacles into the buffer
* `void draw_chicken(int chicken_x, int chicken_y)` - draws the chicken on a tile specified by the x and y coordinates
<br>

__knob_utils.c__

This library is used for reading the player input from the three knobs. As per most other libraries, there is a dedicated initialization function (`init_knobs()`). 
* `void update_knobs()` - updates the knobs and their current cycle
* `int get_red()` - returns the value of the red knob 
* `int get_red_cycle()` - returns the current cycle of the red knob
* `bool get_red_pressed()` - returns a truth value for whether the red knob is pressed

Similarly, you can also call these functions for the green and blue knobs.
<br>
<br>

__light_utils.c__

This library is used for controlling the LEDs on the mzAPO device. This library also needs initialization (`init_lights()`).
* `void reset_rgb_leds()` - resets the LEDs by turning them off
* `void set_rgb_leds_color(uint8_t r, uint8_t g, uint8_t b)` - sets the color of both RGB LEDs to a specified color
* `void rgb_leds_effect1(uint8_t r, uint8_t g, uint8_t b, int blink_num, int delay)` - makes both RGB LEDs blink a certain amount of times with a set duration
* `void led_strip_effect1(int duration)` - makes the LEDs light up from the middle of the strip in two blocks of size 3 with them going in the opposite directions
<br>

__sound_utils.c__

This library is used for playing music or various other sound effects. Initialization is done through the `init_sound()` method.
* `void stop_sound()` - stops all the sounds currently in play
* `void play_song(int song_id)` - plays a short looping song based on its id
* `void sound_effect_train()` - plays the sound effect for when the train is about to arrive
* `void sound_effect_gameover()` - plays the sound effect for when the player dies
* `void play_note(char note, int octave, int duration)` - play a single note
<br>

__main_window.c__

Library for displaying the main menu window. It is necessary to call the `init_main_window()` first.
* `void draw_main_window()` - draws the main menu on the screen with the cursor at the selected option
* `int get_selected()` - returns the index of the currently selected option
* `void destroy_main_window()` - frees all allocated memory used by this library
<br>

__other_windows.c__

Library for displaying several different windows other than the main menu. It is initialized by (`init_windows()`).
* `void goto_score_window()` - switches the view to the score window
* `void goto_tutorial_window()` - switches the tutorial to the score window
* `void goto_gameover_window(int score)` - switches the game-over to the score window
* `void update_scores(int new_score)` - updates the scores table with a new score if necessary
* `void destroy_other_windows()` - frees all allocated memory used by this library
<br>

__components.c__

Library for creating simple wrapper objects for easier manipulation.
* Font - stores the font_descriptor_t as well as the font size and font actual max height
* Label - stores the Font as well as the x and y coordinates of the label, followed by the actual text to be displayed and its color

* `Font *create_font(font_descriptor_t *fdes, double font_size)` - creates a new font based on the given parameters
* `Label *create_label (double x, double y, Font *font, char *text, unsigned short color)` - creates a new label based on the given parameters
<br>

__load_image.c__

This library consists of two main methods for loading images from files.

* `Image* load_image(char *filename)` - get an image with a give name
* `void free_image(Image *img)` - frees all allocated data in `Image` the struct

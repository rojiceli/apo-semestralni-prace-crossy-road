#define _POSIX_C_SOURCE 200112L
#define SQUARE_DIMENSION 40

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <assert.h>
#include <stdbool.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "serialize_lock.h"
#include "font_types.h"
#include "game.h"
#include "draw_utils.h"
#include "knobs_utils.h"
#include "light_utils.h"
#include "sound_utils.h"
#include "main_window.h"
#include "other_windows.h"


void close_program();


int main(int argc, char *argv[]) {

    init_display();
    init_knobs();
    init_lights();
    init_sound();
    init_main_window();
    init_windows();
    reset_rgb_leds();
 
    update_screen();

    led_strip_effect1(1000);
    rgb_leds_effect1(40, 100, 0, 1, 1500);
    play_song(1);


    while (1) {
        draw_background_image();
        draw_main_window();
        
        update_knobs();
        int choice = get_selected();
        
        if (get_green_pressed()) {
            switch (choice)
            {
            case 0:
                stop_sound();
                run();
                break;
            case 1:
                stop_sound();
                play_song(0);
                goto_score_window();
                stop_sound();
                break;
            case 2:
                stop_sound();
                goto_tutorial_window();
                break;
            case 3:
                close_program();
                break;
            default:
                break;
            }
        }
        
        play_song(1);
        update_screen();
    }
    return 0;
}


void close_program(){
    fill_rectangle(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT, 0);
    update_screen();
    destroy_main_window();
    destroy_other_windows();
    stop_sound();
    exit(0);
}

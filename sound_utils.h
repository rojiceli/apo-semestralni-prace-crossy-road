#ifndef SOUND_UTILS_H
#define SOUND_UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>

#include "consts.h"
#include "mzapo_parlcd.h"
#include "mzapo_regs.h"
#include "mzapo_phys.h"

/** @brief The required initiation method for the sound_utils library */
void init_sound();

/** @brief Stops all the sounds currently in play */
void stop_sound();

/** @brief Plays a song based on its id.
 * 
 * @param song_id The ID of the song to be played.
 * @param 0 epic sax guy
 * @param 1 harry potter theme
 */
void play_song(int song_id);

/** @brief Plays the sound effect for when the train is about to arrive */
void sound_effect_train();

/** @brief Plays the sound effect for when the player dies */
void sound_effect_gameover();

/** @brief Play a single note
 * 
 * @param note char for the note to be played (capital letter for normal note, small letter for a sharp i.e. 'f' = F#)
 * @param octave the octave of the note (currently supported 4 or 5)
 * @param duration the amount of time for the note to be played in milliseconds
 */
void play_note(char note, int octave, int duration);

#endif
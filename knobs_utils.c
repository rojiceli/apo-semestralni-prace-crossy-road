#include "knobs_utils.h"

int x_position;
int y_position;

int r_pos;
int g_pos;
int b_pos;

bool r_pressed;
bool g_pressed;
bool b_pressed;

int r_cycle;
int g_cycle;
int b_cycle;

unsigned char *mem_base;

void init_knobs() {
    mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (mem_base == NULL){
        exit(1);
    }

    int r = *(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
    if (!(r&0x7000000)!=0) {
        r_pos = (r>>16) & 0xff;
        g_pos = (r>>8) & 0xff;
        b_pos = r & 0xff;

    }

    r_cycle = 0;
    g_cycle = 0;
    b_cycle = 0;


}

void update_knobs() {
    int r = *(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);

    r_pressed = (r >> 24) & 0x01;
    g_pressed = (r >> 25) & 0x01;
    b_pressed = (r >> 26) & 0x01;

    if (!(r&0x7000000)!=0) {
        int new_r_pos = (r>>16) & 0xff;
        int new_g_pos = (r>>8) & 0xff;
        int new_b_pos = r & 0xff;

        if (r_pos > 200 && new_r_pos < 50) {
            r_cycle += 1;
        } else if (g_pos > 200 && new_g_pos < 50) {
            g_cycle += 1;
        } else if (b_pos > 200 && new_b_pos < 50) {
            b_cycle += 1;
        } else if (new_r_pos > 200 && r_pos < 50) {
            r_cycle -= 1;
        } else if (new_g_pos > 200 && g_pos < 50) {
            g_cycle -= 1;
        } else if (new_b_pos > 200 && b_pos < 50) {
            b_cycle -= 1;
        }

        r_pos = new_r_pos;
        g_pos = new_g_pos;
        b_pos = new_b_pos;
    }
}

int get_red() {
    return r_pos;
}

int get_green() {
    return g_pos;
}

int get_blue() {
    return b_pos;
}

int get_green_cycle(){
    return g_cycle;
}

int get_blue_cycle(){
    return b_cycle;
}

int get_red_cycle(){
    return r_cycle;
}

bool get_red_pressed(){
    return r_pressed;
}

bool get_green_pressed(){
    return g_pressed;
}

bool get_blue_pressed(){
    return b_pressed;
}


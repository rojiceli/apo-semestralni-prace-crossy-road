#include "main_window.h"



Font *font_title;
Font *font_button;

Label *label_title;
Label *label_game;
Label *label_score;
Label *label_tutorial;
Label *label_back;

int selected_btn;

void init_main_window(){
    font_title = create_font(&font_winFreeSystem14x16, 4.);
    font_button = create_font(&font_winFreeSystem14x16, 2.5);
    
    int offset = font_button->font_height + SEPARATOR_HEIGHT;
    label_title = create_label(10, 10, font_title, "Crossy Road", 0xffff);

    label_game = create_label(LEFT_INDENT, TOP_INDENT + 0*offset, font_button, "Game", 0xffff);
    label_score = create_label(LEFT_INDENT, TOP_INDENT + 1*offset, font_button, "Score", 0xffff);
    label_tutorial = create_label(LEFT_INDENT, TOP_INDENT + 2*offset, font_button, "Tutorial", 0xffff);
    label_back = create_label(LEFT_INDENT, TOP_INDENT + 3*offset, font_button, "Back", 0xffff);
}

void draw_main_window(){
    draw_label(label_title);
    draw_label(label_game);
    draw_label(label_score);
    draw_label(label_tutorial);
    draw_label(label_back);

    selected_btn = (get_green() / 8) % 4;
    int btn_y = TOP_INDENT + selected_btn * 45;
    btn_y += (font_button->font_height - CURSOR_HEIGHT) / 2;

    fill_triangle(LEFT_INDENT - CURSOR_HEIGHT - CURSOR_BORDER_WIDTH + 1, 
        btn_y - 2*CURSOR_BORDER_WIDTH, CURSOR_HEIGHT + 4*CURSOR_BORDER_WIDTH, convert_color(255, 255, 255));
    fill_triangle(LEFT_INDENT - CURSOR_HEIGHT, btn_y, CURSOR_HEIGHT, convert_color(0, 255, 0));
}

int get_selected(){
    return selected_btn;
}

void destroy_main_window(){
    free(font_title);
    free(font_button);
    
    free(label_title);
    free(label_game);
    free(label_score);
    free(label_tutorial);
    free(label_back);
}

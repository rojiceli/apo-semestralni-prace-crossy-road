#include "draw_utils.h"

#define MASK_5BIT 0b11111000
#define MASK_6BIT 0b11111100

void draw_pixel(int x, int y, unsigned short color);
int char_width(font_descriptor_t* fdes, int ch);
void draw_char(double x, double y, font_descriptor_t* fdes, char ch, double font_size, uint16_t color);

unsigned char *parlcd_mem_base;
unsigned short *screen;

void init_display() {
    screen = (unsigned short *)malloc(320*480*2);
    
    if (screen == NULL) {
        fprintf(stderr, "Cannot allocate memory for display.\n");
        exit(-1);
    }

    parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);

    if (parlcd_mem_base == NULL) {
        fprintf(stderr, "Cannot allocate memory for display.\n");
        free(screen);
        exit(-1);
    }

    parlcd_hx8357_init(parlcd_mem_base);
    parlcd_write_cmd(parlcd_mem_base, 0x2c);


    for (int i = 0; i < DISPLAY_HEIGHT * DISPLAY_WIDTH; i++) {
        screen[i] = 0;
    }

    update_screen();
}

void update_screen(){
    for (int i = 0; i < DISPLAY_HEIGHT * DISPLAY_WIDTH; i++) {
        parlcd_write_data(parlcd_mem_base, screen[i]);
    }
}

uint16_t convert_color(uint8_t red, uint8_t green, uint8_t blue){
    uint16_t converted_color = 0;
    converted_color = (((red & MASK_5BIT)<<8) + ((green & MASK_6BIT)<<3) + (blue>>3));

    return converted_color;
}

void draw_pixel(int x, int y, unsigned short color) {
  if (x>=0 && x<480 && y>=0 && y<320) {
    screen[x+480*y] = color;
  }
}

void draw_image(int x, int y, char *filename) {
    char folder[100] = IMG_FOLDER;
    strcat(folder, filename);

    Image *img = load_image(folder);

    for (int h = 0; h < img->height; h++) {
        for (int w = 0; w < img->width; w++) {
            uint16_t color = img->img[img->width*h + w];
            if (color != 0xF81F) {
                draw_pixel(x+w, y+h, color);
            }
        }
    }
    free_image(img);
}

void draw_image_fliped(int x, int y, char *filename) {
    char folder[100] = IMG_FOLDER;
    strcat(folder, filename);

    Image *img = load_image(folder);

    for (int h = 0; h < img->height; h++) {
        for (int w = 0; w < img->width; w++) {
            uint16_t color = img->img[img->width*h + w];
            if (color != 0xF81F) {
                draw_pixel((x + (img->width - w)),  y+h, color);
            }
        }
    }

    free_image(img);
}

void draw_background_image() {
    draw_image(0, 0, BG_IMG_FILE);
}

void draw_game_tiles(square* layout[8][12]) {
    square *square;
    int square_type_biome;
    int square_type_img;
    for (int i = 0; i < GRID_HEIGHT; i++) {
        for (int j = 0; j < GRID_WIDTH; j++) {
            char filename[100];

            square = layout[i][j];
            square_type_biome = square->type;
            square_type_img = square->image_mark;

            switch (square_type_biome)
            {
            case 1:
                sprintf(filename, "%s%d%s", "grass", square_type_img, ".ppm");
                break;
            case 2:
                sprintf(filename, "%s%d%s", "road", square_type_img, ".ppm");
                break;
            case 3:
                sprintf(filename, "%s%d%s", "river", square_type_img, ".ppm");
                break;
            case 4:
                sprintf(filename, "%s%d%s", "rail", square_type_img, ".ppm");
                break;
            default:
                fprintf(stderr, "Incorrect biome type: (%i)", square_type_biome);
                exit(-2);
                break;
            }
            draw_image(j*SQUARE_DIMENSION, i*SQUARE_DIMENSION, filename);
        }
    }
}

void draw_obstacles(obstacle *obst_layout[8][12], square* sq_layout[8][12]) {
    obstacle *obst;
    int obst_type;
    int obst_img;
    int obst_part;

    for (int i = 0; i < GRID_HEIGHT; i++) {
        for (int j = 0; j < GRID_WIDTH; j++) {
            char filename[100];

            obst = obst_layout[i][j];
            obst_type = obst->type;
            obst_img = obst->image_mark;
            obst_part = obst->obstacle_part;

            switch (obst_type) {
                case 0: break;
                case 1: break;
                case 2: sprintf(filename, "%s%d%d%s", "car", obst_img, obst_part, ".ppm"); break;
                case 3: sprintf(filename, "%s%d%d%s", "log", obst_img, obst_part, ".ppm"); break;
                case 4: sprintf(filename, "%s%d%d%s", "train", obst_img, obst_part, ".ppm"); break;
                default:
                    fprintf(stderr, "Incorrect biome type: (%i)", obst_type);
                    exit(13);
                    break;
            }

            if ((obst_type != 1) && (obst_type != 0)) {
                if (sq_layout[i][j]->direction) {
                    draw_image_fliped(j*SQUARE_DIMENSION, i*SQUARE_DIMENSION, filename);
                } else {
                    draw_image(j*SQUARE_DIMENSION, i*SQUARE_DIMENSION, filename);
                }
            }
        }
    }
}

void draw_chicken(int chicken_x, int chicken_y) {
    int chicken_pix_pos_x = chicken_x * SQUARE_DIMENSION;
    int chicken_pix_pos_y = chicken_y * SQUARE_DIMENSION;

    draw_image(chicken_pix_pos_x, chicken_pix_pos_y - 10, "chicken.ppm");
}

void fill_rectangle(double x, double y, double width, double height, uint16_t color) {
    double x2 = x + width;
    double y2 = y + height;

    int startX = (int)floor(x);
    int endX = (int)floor(x2);
    int startY = (int)floor(y);
    int endY = (int)floor(y2);

    for (int y = startY; y < endY; y++) {
        for (int x = startX; x < endX; x++) {
            draw_pixel(x, y, color);
        }
    }
}

void fill_triangle(int x, int y, int height, uint16_t color){

    for (size_t j = 0; j < height / 2; j++) {
        for (size_t i = 0; i < height - 2*j; i++) {
            draw_pixel(x + j, y + j + i, color);
        }
    }
}

void draw_char(double x, double y, font_descriptor_t* fdes, char ch, double font_size, uint16_t color) {
    int index = ch - fdes->firstchar;
    int width = char_width(fdes, ch);

    for (size_t i = 0; i < fdes->height; i++) {
        for (size_t k = 0; k < width; k++) {
            bool isThere = (fdes->bits[index * 16 + i] << k) & 0x8000;
            if(isThere){
                fill_rectangle(x + (font_size * k), y + (font_size * i), font_size, font_size, color);
            }
        }
    }
}

int char_width(font_descriptor_t* fdes, int ch) {
    int width = 0;
    if ((ch >= fdes->firstchar) && (ch-fdes->firstchar < fdes->size)) {
        ch -= fdes->firstchar;
        if (!fdes->width) {
        width = fdes->maxwidth;
        } else {
        width = fdes->width[ch];
        }
    }
    return width;
}

void draw_string(double x, double y, char *str, Font *font, uint16_t color){
    for (int i = 0; i < strlen(str); i++) {
        char c = str[i];
        draw_char(x, y, font->fdes, c, font->font_size, color);
        x += char_width(font->fdes, c) * font->font_size;
    }
}

void draw_string_centered(double y, char *str, Font *font, uint16_t color){
    int len = strlen(str);
    int x = (480 - (font->fdes->maxwidth * font->font_size) * len) / 2;
    draw_string(x, y, str, font, color);
}

void draw_label(Label *label){
    draw_string(label->x, label->y, label->text, label->font, label->color);
}

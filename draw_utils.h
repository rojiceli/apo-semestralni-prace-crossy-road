#ifndef DRAW_UTILS
#define DRAW_UTILS


#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <string.h>

#include "mzapo_regs.h"
#include "mzapo_phys.h"
#include "consts.h"
#include "load_image.h"
#include "font_types.h"
#include "components.h"
#include "game.h"

/** @brief The required initiation method for the draw_utils library */
void init_display();

/** @brief Redraw the display with the changes stored in the buffer */
void update_screen();

/** @brief Converts a color from classic 16bit rgb to rgb565
 * 
 * @param red the red color component (0 - 255)
 * @param green the green color component (0 - 255)
 * @param blue the blue color component (0 - 255)
*/
uint16_t convert_color(uint8_t red, uint8_t green, uint8_t blue);

/** @brief Draws an image into the buffer
 * 
 * @param x the x coordinate of where the image should be drawn
 * @param y the y coordinate of where the image should be drawn
 * @param filename the file name of the image
*/
void draw_image(int x, int y, char *filename);

/** @brief Draws an image into the buffer, but flipped horizontaly
 * 
 * @param x the x coordinate of where the image should be drawn
 * @param y the y coordinate of where the image should be drawn
 * @param filename the file name of the image
*/
void draw_image_fliped(int x, int y, char *filename);

/** @brief Draws the background image into the buffer*/
void draw_background_image();

/** @brief Draws the filled rectangle into the buffer
 * 
 * @param x the x coordinate of where the rectangle should be drawn
 * @param y the y coordinate of where the rectangle should be drawn
 * @param width the height of the rectangle
 * @param height the width of the rectangle
 * @param color the color of the rectangle (in rgb565)
*/
void fill_rectangle(double x, double y, double width, double height, uint16_t color);

/** @brief Draws the filled triangle |> into the buffer
 * 
 * @param x the x coordinate of where the triangle should be drawn
 * @param y the y coordinate of where the triangle should be drawn
 * @param height the width of the triangle
 * @param color the color of the triangle (in rgb565)
*/
void fill_triangle(int x, int y, int height, uint16_t color);


/** @brief Draws a string into the buffer
 * 
 * @param x the x coordinate of where the string should be drawn
 * @param y the y coordinate of where the string should be drawn
 * @param *text the text to be displayed
 * @param *font pointer to the Font struct in which the text should be drawn
 * @param color the color of the text (in rgb565)
*/
void draw_string(double x, double y, char *str, Font *font, uint16_t color);


/** @brief Draws a horizontaly centered string into the buffer
 * 
 * @param x the x coordinate of where the string should be drawn
 * @param *text the text to be displayed
 * @param *font pointer to the Font struct in which the text should be drawn
 * @param color the color of the text (in rgb565)
*/
void draw_string_centered(double y, char *str, Font *font, uint16_t color);

/** @brief Draws a Label into the buffer
 * 
 * @param x the x coordinate of where the string should be drawn
 * @param *text the text to be displayed
 * @param *font pointer to the Font struct in which the text should be drawn
 * @param color the color of the text (in rgb565)
*/
void draw_label(Label *label);

void draw_game_tiles(square *layout[8][12]);
void draw_obstacles(obstacle *obst_layout[8][12], square* sq_layout[8][12]);
void draw_chicken(int chicken_x, int chicken_y);

#endif
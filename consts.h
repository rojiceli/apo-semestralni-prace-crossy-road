#ifndef CONSTS
#define CONSTS

#define DISPLAY_WIDTH 480
#define DISPLAY_HEIGHT 320
#define INVISIBLE_COLOR 0xF81F
#define SQUARE_DIMENSION 40
#define KNOBS_DIFFERENCE 10
#define GRID_WIDTH 12
#define GRID_HEIGHT 8

#define LEFT_INDENT 50
#define TOP_INDENT 80
#define SEPARATOR_HEIGHT 5
#define CURSOR_HEIGHT 25
#define CURSOR_BORDER_WIDTH 4

#define TUT_ANIM_LENGTH 50
#define TUT_ANIM_FPS 8
#define DEATH_ANIM_LENGTH 20
#define DEATH_ANIM_FPS 20
#define GLOBAL_FPS 30

#define NO_OBSTACLE_TYPE 0
#define GRASS_TYPE 1
#define ROAD_TYPE 2
#define RIVER_TYPE 3
#define RAIL_TYPE 4

#define VOLUME 0

#define IMG_FOLDER "/tmp/houdefi1/images/"
#define HIGH_SCORE_FILE "/tmp/houdefi1/highscores.txt"
#define BG_IMG_FILE "background_grass.ppm"

#include <stdbool.h>

// direction = 0 => překážky jedou ->
// direction = 1 => překážky jedou <-

typedef struct {
    int type;
    int image_mark;
    bool direction;
    int obstacle_ttl;
    int spacing;
    bool starting_obstacle;
} square;

typedef struct {
    int type;
    int image_mark;
    int obstacle_part;
    int ttl;
} obstacle;


#endif
#ifndef LIGHT_UTILS_H
#define LIGHT_UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>

#include "consts.h"
#include "mzapo_parlcd.h"
#include "mzapo_regs.h"
#include "mzapo_phys.h"


/** @brief The required initiation method for the light_utils library */
void init_lights();

/** @brief Resets the leds by turning them off */
void reset_rgb_leds();

/** @brief sets the color of both rgb leds to a specified color
 * 
 * @param r the red color component (0 - 255)
 * @param g the green color component (0 - 255)
 * @param b the blue color component (0 - 255)
*/
void set_rgb_leds_color(uint8_t r, uint8_t g, uint8_t b);

/** @brief makes both rgb leds blink certain amount of times with set duration
 * 
 * @param r the red color component (0 - 255)
 * @param g the green color component (0 - 255)
 * @param b the blue color component (0 - 255)
 * @param blink_num number of times the led should blink
 * @param delay the duration of the led being in one state in milliseconds
*/
void rgb_leds_effect1(uint8_t r, uint8_t g, uint8_t b, int blink_num, int delay);

/** @brief makes leds light up from the middle of the strip in two blocks of size 3 which each goes a different
 * @param duration duration of the whole effect in  milliseconds
*/
void led_strip_effect1(int duration);

#endif
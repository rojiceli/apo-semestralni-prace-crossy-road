#include "other_windows.h"

Font *font_title_other;
Font *font_text;
Font *font_text_small;

Label *label_score_title;
Label *label_score_record;
Label *label_tutorial_title;
Label *label_gameover_title;


int scores[5] = {0,0,0,0,0};


void init_windows(){
    font_title_other = create_font(&font_winFreeSystem14x16, 4.0);
    font_text = create_font(&font_rom8x16, 2.5);
    font_text_small = create_font(&font_rom8x16, 1.8);
    
    label_score_title = create_label(80, 10, font_title_other, "Score board", 0xffff);
    label_tutorial_title = create_label(10, 10, font_title_other, "Tutorial", 0xffff);
    label_gameover_title = create_label(60, 20, font_title_other, "GAME OVER!", 0xffff);
    label_score_record = create_label(110, 0, font_text, "", 0xffff);

    FILE *highscore_file = fopen(HIGH_SCORE_FILE, "r");
    if (!highscore_file) {
        fprintf(stderr, "Cannot open file: %s\n", HIGH_SCORE_FILE);
    }

    for (size_t i = 0; i < 5; i++)
    {
        fscanf(highscore_file, "%d\n", &scores[i]);
    }

    fclose(highscore_file);
}

void goto_score_window(){
    parlcd_delay(200);
    update_knobs();
    draw_background_image();
    draw_string(80, 10, "Score board", font_title_other, 0xffff);
    draw_label(label_score_title);

    for (size_t i = 0; i < 5; i++) {
        sprintf(label_score_record->text, "%d. %10d", i, scores[i]);
        label_score_record->y = TOP_INDENT + i * (font_text->font_height + SEPARATOR_HEIGHT);
        draw_label(label_score_record);
    }

    update_screen();

    while (!get_green_pressed()) {
        update_knobs();
    }

    parlcd_delay(200);
    update_knobs();
}

void goto_tutorial_window(){
    parlcd_delay(200);
    update_knobs();

    int real_frame_id = 0;
    int anim_frame_id = 1;

    update_knobs();
    while (!get_green_pressed()) {
        update_knobs();
        draw_background_image();

        char frame_name[50];
        sprintf(frame_name, "tutorial_animation/tutorial_frame_%04d.ppm", anim_frame_id);

        if(++real_frame_id > GLOBAL_FPS / TUT_ANIM_FPS){
            if(++anim_frame_id > TUT_ANIM_LENGTH) {
                anim_frame_id = 1;
            }
            real_frame_id = 0;
        }

        if(anim_frame_id == 39 || anim_frame_id == 41 || anim_frame_id == 43){
            set_rgb_leds_color(255, 0, 0);
        } else {
            reset_rgb_leds();
        }

        draw_image(0, 0, frame_name);
        draw_label(label_tutorial_title);
        update_screen();
        parlcd_delay(1000 / GLOBAL_FPS);
    }

    parlcd_delay(200);
    update_knobs();
}

void update_scores(int new_score){
    int last = new_score;
    FILE *highscore_file = fopen(HIGH_SCORE_FILE, "w");

    for (size_t i = 0; i < 5; i++) {
        if(last > scores[i]) {
            int tmp = last;
            last = scores[i];
            scores[i] = tmp;
        }
        fprintf(highscore_file, "%d\n", scores[i]);
    }

    fclose(highscore_file);
}

void goto_gameover_window(int score){

    int real_frame_id = 0;
    int anim_frame_id = 1;

    while (anim_frame_id <= DEATH_ANIM_LENGTH) {
        char frame_name[45];
        sprintf(frame_name, "death_animation/death_frame_%04d.ppm", anim_frame_id);

        if(++real_frame_id > GLOBAL_FPS / DEATH_ANIM_FPS) {
            anim_frame_id ++;
            real_frame_id = 0;
        }

        draw_image(0, 0, frame_name);
        update_screen();
        parlcd_delay(1000 / GLOBAL_FPS);
    }


    update_scores(score);

    draw_background_image();
    fill_rectangle(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT, convert_color(187, 0, 0));
    draw_label(label_gameover_title);

    char text[50];
    sprintf(text, "Your score: %d", score);
    draw_string_centered(120, text, font_text, 0xffff);
    
    sprintf(text, "Points to best: %d", scores[0] - score);
    draw_string_centered(165, text, font_text, 0xffff);
    
    draw_string_centered(240, "Press        ", font_text_small, 0xffff);
    draw_string_centered(240, "      [green]", font_text_small, convert_color(50, 255, 0));
    draw_string_centered(265, "to play again", font_text_small, 0xffff);

    update_screen();

    while (!get_green_pressed()) {
        update_knobs();
    }

    parlcd_delay(200);
    update_knobs();
}

void destroy_other_windows(){
    free(font_title_other);
    free(font_text);
    free(font_text_small);

    free(label_score_title);
    free(label_score_record);
    free(label_tutorial_title);
    free(label_gameover_title);
}


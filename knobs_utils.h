#ifndef KNOBS_UTILS
#define KNOBS_UTILS

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "mzapo_regs.h"
#include "mzapo_phys.h"

/** @brief The required initiation method for the knobs_utils library */
void init_knobs();

/** @brief Updates the values of all the knobs */
void update_knobs();

/** @brief Get the last value of red knob */
int get_red();
/** @brief Get the last value of green knob */
int get_green();
/** @brief Get the last value of blue knob */
int get_blue();

/** @brief Get the last cycle in which the red knob was */
int get_red_cycle();
/** @brief Get the last cycle in which the green knob was */
int get_green_cycle();
/** @brief Get the last cycle in which the blue knob was */
int get_blue_cycle();

/** @brief Get the last state in which the red knob was */
bool get_red_pressed();
/** @brief Get the last state in which the green knob was */
bool get_green_pressed();
/** @brief Get the last state in which the blue knob was */
bool get_blue_pressed();

#endif

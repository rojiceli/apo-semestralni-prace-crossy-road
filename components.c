#include "components.h"


Font *create_font(font_descriptor_t *fdes, double font_size){
    Font *font = malloc(sizeof(Font));
    font->fdes = fdes;
    font->font_size = font_size;
    font->font_height = fdes->height * font_size;
    return font;
}

Label *create_label(double x, double y, Font *font, char *text, unsigned short color){
    Label *label = malloc(sizeof(Label));
    label->x = x;
    label->y = y;
    label->font = font;
    label->text = text;
    label->color = color;
    return label;
}
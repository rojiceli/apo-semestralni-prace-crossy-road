#include "game.h"

int chicken_x_position;
int chicken_y_position;
int last_absolut_r;
int last_absolut_b;
int score;

int lines_type[8];
square *background_layout[8][12];
obstacle *obstacle_layout[GRID_HEIGHT][GRID_WIDTH];

bool is_rail_there = false;
bool is_train_on_rail = false;
bool is_chicken_on_log = false;
int lenght_of_train = 0;
int counter_till_the_train = 0;

void init_game();
void init_background_array();
void init_obstacles_array();
int generate_line_type();
obstacle *create_obstacle(int type, int image_mark, int part);
square *create_square(int type, int image_mark, bool direction, int obstacle_ttl, int spacing);
int decide_if_the_chicken_should_move();
void move_chicken(int direction);
void move_background_layout();
void move_obstacles_background();
void update_obstacles();
void change_starting_obstacle(int row);
void shift_obstacles_right(obstacle *new_obstacle, int row);
void shift_obstacles_left(obstacle *new_obstacle, int row);
void reset_ttl_in_row(int row, int ttl);
bool check_collision();
void free_layout_arrays();


void init_game() {
    chicken_x_position = 5;
    chicken_y_position = 5;
    score = 0;

    int new_r_pos = get_red();
    int new_b_pos = get_blue();
    int new_r_cycle = get_red_cycle();
    int new_b_cycle = get_blue_cycle();

    last_absolut_r = (new_r_cycle * 256) + new_r_pos;
    last_absolut_b = (new_b_cycle * 256) + new_b_pos;
    
    init_background_array();
    init_obstacles_array();
}

void init_background_array() {
    int line_type;
    bool direction = true;
    int obstacle_ttl = 0; 
    int spacing = 8;
    for (int i = 0; i < GRID_HEIGHT; i++) {
        
        if (i < 4) {
            line_type = generate_line_type();
            if (line_type == RAIL_TYPE) {
                is_rail_there = true;
            }

        } else {
            line_type = 1;
        }

        if (line_type != 1) {
            direction = rand() % 2;
            obstacle_ttl = rand() % 6 + 8;
            spacing = rand() % 3 + 3;
        }

        if (line_type == RAIL_TYPE) {
            obstacle_ttl = 2;
        }
        
        for (int j = 0; j < GRID_WIDTH; j++) {
            int num = rand() % 3 + 1;
            square *sq = create_square(line_type, num, direction, obstacle_ttl, spacing);
            background_layout[i][j] = sq;
        }
    }
}

void init_obstacles_array() {
    for (int i = 0; i < GRID_HEIGHT; i++) {
        for (int j = 0; j < GRID_WIDTH; j++) {
            obstacle_layout[i][j] = create_obstacle(0, 0, 0);
        }
    }
}

void run() {
    init_game();

    while(1){
        update_knobs();

        int decision = decide_if_the_chicken_should_move();
        move_chicken(decision);
        update_obstacles();
        draw_game_tiles(background_layout);
        draw_obstacles(obstacle_layout, background_layout);
        draw_chicken(chicken_x_position, chicken_y_position);

        update_screen();

        is_chicken_on_log = (obstacle_layout[chicken_y_position][chicken_x_position]->type == RIVER_TYPE);


        if (!check_collision()) {
            free_layout_arrays();
            sound_effect_gameover();
            goto_gameover_window(score);
            break;
        }
    }
}

int generate_line_type() {
    int new_line_type;

    do {
    int random = rand() % 100 + 1;
    int grass_upper_bound;
    int road_upper_bound;
    int river_upper_bound;

    if (score < 10) {
        grass_upper_bound = 40;
        road_upper_bound = 55;
        river_upper_bound = 75;

    } else if (score >= 10 && score < 25) {
        grass_upper_bound = 30;
        road_upper_bound = 65;
        river_upper_bound = 85;

    } else {
        grass_upper_bound = 15;
        road_upper_bound = 55;
        river_upper_bound = 70;
    }

    if (random <= grass_upper_bound) {
        new_line_type = GRASS_TYPE;
    } else if (random <= road_upper_bound) {
        new_line_type = ROAD_TYPE;
    } else if (random <= river_upper_bound) {
        new_line_type = RIVER_TYPE;
    } else {
        new_line_type = RAIL_TYPE;
    }

    } while (new_line_type == RAIL_TYPE && is_rail_there);

    return new_line_type;
}

obstacle *create_obstacle(int type, int image_mark, int part) {
    obstacle *new_obstacle = malloc(sizeof(obstacle));

    new_obstacle->type = type;
    new_obstacle->image_mark = image_mark;
    new_obstacle->obstacle_part = part;
    new_obstacle->ttl = 0;

    return new_obstacle;
}

square *create_square(int type, int image_mark, bool direction, int obstacle_ttl, int spacing) {
    square *new_sqaure = malloc(sizeof(square));

    new_sqaure->type = type;
    new_sqaure->image_mark = image_mark;
    new_sqaure->direction = direction;
    new_sqaure->obstacle_ttl = obstacle_ttl;
    new_sqaure->spacing = spacing;
    new_sqaure->starting_obstacle = false;

    return new_sqaure;
}

int decide_if_the_chicken_should_move() {
    int new_r_pos = get_red();
    int new_b_pos = get_blue();
    int new_r_cycle = get_red_cycle();
    int new_b_cycle = get_blue_cycle();
    int new_absolut_r = (new_r_cycle * 256) + new_r_pos;
    int new_absolut_b = (new_b_cycle * 256) + new_b_pos;

    int red_difference  = abs(new_absolut_r - last_absolut_r);
    int blue_difference = abs(new_absolut_b - last_absolut_b);

    int ret;

    if (red_difference >= KNOBS_DIFFERENCE) {
        if (new_absolut_r > last_absolut_r) {
            ret = 1;
        } else {
            ret = 3;
        }
        last_absolut_r = new_absolut_r;

    } else if (blue_difference >= KNOBS_DIFFERENCE) {
        if (new_absolut_b > last_absolut_b) {
            ret = 2;
        } else {
            ret = 4;
        }
        last_absolut_b = new_absolut_b;
    } else {
        ret = 0;
    }
    return ret;
}

void move_chicken(int direction) {
    if (direction != 0) {
        
        switch (direction)
        {
        case 1:
            chicken_x_position += 1;
            break;
        case 2:
            chicken_y_position += 1;
            break;
        case 3:
            chicken_x_position -= 1;
            break;
        case 4:
            chicken_y_position -= 1;
            break;
        default:
            break;
        }

        if (chicken_x_position > 11) {
            chicken_x_position = 11;
        } else if (chicken_y_position > 7) {
            chicken_y_position = 7;
        } else if (chicken_x_position < 0) {
            chicken_x_position = 0;
        } else if (chicken_y_position < 4) {
            chicken_y_position = 4;
            move_background_layout();
            move_obstacles_background();
        }
    }
}

void move_background_layout() {
    square *new_array[12];

    if (is_rail_there) {
        if (background_layout[GRID_HEIGHT-1][0]->type == RAIL_TYPE) {
            is_rail_there = false;
        }
    }

    int new_line_type = generate_line_type();

    if (new_line_type == RAIL_TYPE) {
        is_rail_there = true;
    }

    bool direction = 0;
    int obstacle_ttl = 0;
    int spacing = 8;

    if (new_line_type != GRASS_TYPE) {
        direction = rand() % 2;
        obstacle_ttl = rand() % 6 + 8;
        spacing = rand() % 3 + 3;
    }

    if (new_line_type == RAIL_TYPE) {
        obstacle_ttl = 2;
    }

    for (int i = 0; i < GRID_WIDTH; i++) {
        int num = rand() % 3 + 1;
        new_array[i] = create_square(new_line_type, num, direction, obstacle_ttl, spacing);
    }

    memmove(&background_layout[1], &background_layout[0], sizeof(background_layout) - sizeof(background_layout[0]));
    memcpy(&background_layout[0], new_array, sizeof(background_layout[0]));
    score++;
}

void move_obstacles_background() {
    obstacle *new_row[12];

    for (int i = 0; i < GRID_WIDTH; i++) {
        new_row[i] = create_obstacle(0, 0, 0);
    }

    memmove(&obstacle_layout[1], &obstacle_layout[0], sizeof(obstacle_layout) - sizeof(obstacle_layout[0]));
    memcpy(&obstacle_layout[0], new_row, sizeof(obstacle_layout[0]));
}

void update_obstacles() {
    for (int i = 0; i < GRID_HEIGHT; i++) {
        int row_type = background_layout[i][0]->type;
        if (row_type != GRASS_TYPE) {
            obstacle **obstacle_row = obstacle_layout[i];
            int ttl_in_row = 0;

            for (int j = 0; j < GRID_WIDTH; j++) {
                int obstacle_type = obstacle_row[j]->type;
                if (obstacle_type != 0) {
                    obstacle_row[j]->ttl -= 1;
                    ttl_in_row = obstacle_row[j]->ttl;
                }
            }

            if (ttl_in_row < 1) {
                int no_type = NO_OBSTACLE_TYPE;
                int no_img_mark = 0;
                int no_part = 0;

                bool direction = background_layout[i][0]->direction;

                if (background_layout[i][0]->starting_obstacle) {
                    int old_obst_pos;
                    
                    if (direction) {
                        old_obst_pos = GRID_WIDTH - 1;

                    } else {
                        old_obst_pos = 0;
                    }

                    obstacle *old_obstacle = obstacle_row[old_obst_pos];

                    no_type = old_obstacle->type;
                    no_img_mark = old_obstacle->image_mark;
                    no_part = 2;

                    if (no_type == RAIL_TYPE) {
                        lenght_of_train++;
                    }

                    if (no_type == RAIL_TYPE && lenght_of_train > 11) {
                        change_starting_obstacle(i);
                        lenght_of_train = 0;
                        counter_till_the_train = 40;
                    }

                    if (no_type == RIVER_TYPE && ((rand() % 3) == 0) )  {
                        change_starting_obstacle(i);
                    }

                    if (no_type != RAIL_TYPE && no_type != RIVER_TYPE) {
                        change_starting_obstacle(i);
                    }

                } else if ((counter_till_the_train == 0) && row_type == RAIL_TYPE) {
                    no_type = RAIL_TYPE;
                    no_img_mark = rand() % 3 + 1;
                    no_part = 1;
                    change_starting_obstacle(i);

                } else if ((rand() & 100) < 30) {
                    no_type = background_layout[i][0]->type;
                    no_img_mark = rand() % 3 + 1;
                    no_part = 1;
                    change_starting_obstacle(i);

                    if (no_type == RAIL_TYPE && (counter_till_the_train != 0 || is_train_on_rail)) {
                        no_type = NO_OBSTACLE_TYPE;
                        change_starting_obstacle(i);
                    }

                } else {
                    no_type = NO_OBSTACLE_TYPE;
                    no_img_mark = 0;
                    no_part = 0;
                }

                obstacle *new_obstacle = create_obstacle(no_type, no_img_mark, no_part);

                if (row_type == RAIL_TYPE && no_type != RAIL_TYPE) {
                    counter_till_the_train--;
                }

                if (no_type == RAIL_TYPE) {
                    is_train_on_rail = true;
                }

                if (counter_till_the_train == 10) {
                    sound_effect_train();
                    rgb_leds_effect1(255, 0, 0, 3, 250);
                }

                if (direction) {
                    shift_obstacles_right(new_obstacle, i);
                } else {
                    shift_obstacles_left(new_obstacle, i);
                }
                reset_ttl_in_row(i, background_layout[i][0]->obstacle_ttl);
            }
        }
    }
}

void change_starting_obstacle(int row) {
    bool new_starting_obst = !(background_layout[row][0]->starting_obstacle);

    for (int i = 0; i < GRID_WIDTH; i ++) {
        background_layout[row][i]->starting_obstacle = new_starting_obst;
    }
}

void shift_obstacles_right(obstacle *new_obstacle, int row) {
    if (obstacle_layout[row][0]->type == RAIL_TYPE && obstacle_layout[row][1]->type != RAIL_TYPE) {
        is_train_on_rail = false;
    }


    for (int i = 1; i < GRID_WIDTH; i++) {
        obstacle_layout[row][i-1] = obstacle_layout[row][i];
    } 
    obstacle_layout[row][GRID_WIDTH-1] = new_obstacle;
    if (is_chicken_on_log && row == chicken_y_position) {
        move_chicken(3);
    }
}

void shift_obstacles_left(obstacle *new_obstacle, int row) {
    obstacle *tmp = obstacle_layout[row][0];
    obstacle *new_tmp;

    if (obstacle_layout[row][GRID_WIDTH-1]->type == RAIL_TYPE && obstacle_layout[row][GRID_WIDTH-2]->type != RAIL_TYPE) {
        is_train_on_rail = false;
    }

    for (int i = 0; i < GRID_WIDTH - 1; i++) {
        new_tmp = obstacle_layout[row][i+1];
        obstacle_layout[row][i+1] = tmp;
        tmp = new_tmp;
    } 
    obstacle_layout[row][0] = new_obstacle;
    if (is_chicken_on_log && row == chicken_y_position) {
        move_chicken(1);
    }
}

void reset_ttl_in_row(int row, int ttl) {
    for (int i = 0; i < GRID_WIDTH; i++) {
        if (obstacle_layout[row][i]->type != 0) {
            obstacle_layout[row][i]->ttl = ttl;
        }
    }
}

bool check_collision() {
    square *sq = background_layout[chicken_y_position][chicken_x_position];
    obstacle *obst = obstacle_layout[chicken_y_position][chicken_x_position];

    if (sq->type == 3) return obst->type != 0;
    else return obst->type == 0;
}

void free_layout_arrays() {
    for (int i = 0; i < GRID_HEIGHT; i++) {
        for (int j = 0; j < GRID_WIDTH; j++) {
            free(background_layout[i][j]);
            free(obstacle_layout[i][j]);
        }
    }
}
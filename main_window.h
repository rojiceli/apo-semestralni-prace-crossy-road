#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include "components.h"
#include "knobs_utils.h"
#include "font_types.h"
#include "draw_utils.h"
#include "consts.h"


/** @brief The required initiation method for the main_window library */
void init_main_window();

/** @brief Draws the main window to the buffer */
void draw_main_window();

/** @brief Returns the index of the currently selected button */
int get_selected();

/** @brief Frees all allocated memory*/
void destroy_main_window();

#endif
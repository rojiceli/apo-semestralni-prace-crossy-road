#ifndef OTHER_WINDOW_H
#define OTHER_WINDOW_H

#include <stdbool.h>
#include <stdio.h>
#include <time.h>
#include "mzapo_parlcd.h"

#include "components.h"
#include "knobs_utils.h"
#include "light_utils.h"
#include "sound_utils.h"
#include "draw_utils.h"
#include "load_image.h"
#include "consts.h"


/** @brief The required initiation method for the other_windows library */
void init_windows();

/** @brief Switches the view to the score window (upon exiting this window, return to just behind this function call) */
void goto_score_window();

/** @brief Switches the tutorial to the score window (upon exiting this window, return to just behind this function call) */
void goto_tutorial_window();

/** @brief Switches the gameover to the score window (upon exiting this window, return to just behind this function call) */
void goto_gameover_window(int score);

/** @brief Updates the scores table with a new score (if necessary)*/
void update_scores(int new_score);

/** @brief Frees all allocated memory*/
void destroy_other_windows();

#endif
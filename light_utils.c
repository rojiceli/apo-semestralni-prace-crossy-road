#include "light_utils.h"


unsigned char *mem_base_leds;

void *rgb_effect1_thread(void *vargp);
void *ls_effect1_thread(void *vargp);

typedef struct ThreadArgs {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    int blink_num;
    int delay;
} rgb_effect1_args;

void init_lights(){
    mem_base_leds = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
}

void reset_rgb_leds(){
    *(volatile uint32_t*)(mem_base_leds + SPILED_REG_LED_RGB1_o) = 0x00000000;
    *(volatile uint32_t*)(mem_base_leds + SPILED_REG_LED_RGB2_o) = 0x00000000;
}

void set_rgb_leds_color(uint8_t r, uint8_t g, uint8_t b){
    uint32_t color = 0;
    uint32_t i;

    i = r;
    color = color | (i << 16);
    i = g;
    color = color | (i << 8);
    i = b;
    color = color | i;

    *(volatile uint32_t*)(mem_base_leds + SPILED_REG_LED_RGB1_o) = color;
    *(volatile uint32_t*)(mem_base_leds + SPILED_REG_LED_RGB2_o) = color;
}

void rgb_leds_effect1(uint8_t r, uint8_t g, uint8_t b, int blink_num, int delay){
    rgb_effect1_args* args = (rgb_effect1_args*)malloc(sizeof(rgb_effect1_args));
    args->r = r;
    args->g = g;
    args->b = b;
    args->blink_num = blink_num;
    args->delay = delay;
    pthread_t thread_id;
    pthread_create(&thread_id, NULL, rgb_effect1_thread, (void*)args);
}

void led_strip_effect1(int duration){
    pthread_t thread_id;
    pthread_create(&thread_id, NULL, ls_effect1_thread, (void*)&duration);
}

void *rgb_effect1_thread(void *vargp) {
    rgb_effect1_args* args = (rgb_effect1_args*)vargp;

    struct timespec delay;
    delay.tv_sec = args->delay / 1000;
    delay.tv_nsec = (args->delay % 1000) * 1000000;

    for (size_t i = 0; i < args->blink_num; i++){
        set_rgb_leds_color(args->r, args->g, args->b);
        nanosleep(&delay, NULL);
        reset_rgb_leds();
        nanosleep(&delay, NULL);
    }
    
    free(vargp);
    return NULL;
} 

void *ls_effect1_thread(void *vargp) {
    int duration = *(int*)vargp;

    uint32_t led_strip = 0;

    uint32_t effect1 = 0x00070000;
    uint32_t effect2 = 0x0000E000;


    for (int i = 0; i < 20; i++) {
        *(volatile uint32_t*)(mem_base_leds + SPILED_REG_LED_LINE_o) = led_strip;
        effect1 <<= 1;
        effect2 >>= 1;
        led_strip = effect1 | effect2;
        parlcd_delay(duration / 20);
    }

    return NULL;
} 
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "load_image.h"

uint8_t* read_ppm(uint8_t *img, int size, FILE *f);

Image *init_image(int width, int height) {
    Image *img = (Image *)malloc(sizeof(Image));

    if (img == NULL) {
        fprintf(stderr, "Cannot allocate memory for image.\n");
        exit(-1);
    }

    img->width = width;
    img->height = height;
    img->size = width * height;
    img->img = malloc(width*height*sizeof(uint16_t));

    return img;
}

Image* load_image(char *filename) {

    FILE *f = fopen(filename, "rb");

    if (!f) {
        fprintf(stderr, "Cannot open file (%s).\n", filename);
        exit(-2);
    } else {
        Image *converted_img = NULL;

        int w, h, mi = 0;
        char header[3];
        if (fscanf(f, "%2s %d %d %d\n", header, &w, &h, &mi) == 4){
            int saved_pixels = 0;
            int size_of_origin_array = w * h * 3;

            converted_img = init_image(w, h);
            uint8_t *origin_img = malloc(size_of_origin_array * sizeof(uint8_t));
            origin_img = read_ppm(origin_img, size_of_origin_array, f);

            for (int i = 0; i < size_of_origin_array; i+=3) {
                uint8_t red = origin_img[i];
                uint8_t green = origin_img[i+1];
                uint8_t blue = origin_img[i+2];

                converted_img->img[saved_pixels] = convert_color(red, green, blue);
                saved_pixels++;
            }
            free(origin_img);
        }
        fclose(f);

        return converted_img;  
    }  
}

uint8_t* read_ppm(uint8_t *img, int size, FILE *f) {
    size_t bytes_read = fread(img, 1, size, f);

    if (bytes_read < size){
        fprintf(stderr, "Image have not been loaded properly! Read only: %d bytes\n", bytes_read);
        exit(-1);
    }
    return img;
}

void free_image(Image *img) {
    free(img->img);
    free(img);
}






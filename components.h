#ifndef COMPONENTS_H
#define COMPONENTS_H

#include <stdlib.h>

#include "font_types.h"

typedef struct Font {
    font_descriptor_t *fdes;
    double font_size;
    double font_height;
} Font;

typedef struct Label {
    double x;
    double y;
    Font *font;
    char *text;
    unsigned short color;
} Label;

/** @brief Creates a new font based on the given parametrs
 * 
 * @param *fdes pointer to the font_descriptor_t struct
 * @param font_size a float number indicating the size of the font
 */
Font *create_font(font_descriptor_t *fdes, double font_size);

/** @brief Creates a new label based on the given parametrs
 * 
 * @param x the x coordinate of where the label should be placed on the screen
 * @param y the y coordinate of where the label should be placed on the screen
 * @param *font the pointer to the Font struct used for the font of the label
 * @param *text the text to be displayed on the label
 * @param color the color of the text
*/
Label *create_label (double x, double y, Font *font, char *text, unsigned short color);

#endif
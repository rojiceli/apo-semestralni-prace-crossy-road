#ifndef LOAD_IMAGE_H
#define LOAD_IMAGE_H

#include <stdint.h>
#include <stdio.h>
#include "draw_utils.h"

typedef struct {
    uint16_t *img;
    int width, height, size;
} Image;

/** @brief Get an image with a give name
 * 
 * @param filename the name of the file to be read
 * 
 * @return the Image struct 
*/
Image* load_image(char *filename);


/** @brief Frees all allocated data in the struct
 * 
 * @param *img pointer to the image to be freed
*/
void free_image(Image *img);

#endif